import React from 'react';
import { AiOutlinePlus } from 'react-icons/ai';

import './AddTaskStyles.scss';

/**
 * @component AddTask добавление задачи с датой
 * @param {PropType} props в компонент передаются props из TodoList, где хранятся чтение данных с fb, состояния input, даты и функция добавления задачи и даты
 * @returns form c возможностью добавить задачу и задать дату для нее
 */

const AddTask = ({ input, addTask, changeInput, date, setDate }) => {
  return (
    <>
      <form onSubmit={addTask}>
        <div className='task-input'>
          <input
            value={input}
            onChange={changeInput}
            className='input-inner'
            placeholder='add task'
            type='text'
          />
          <input
            type='date'
            className='input-date'
            value={date}
            onChange={(e) => setDate(e.target.value)}
          />
          <AiOutlinePlus onClick={addTask} className='task-add' />
        </div>
      </form>
    </>
  );
};

export default AddTask;
