import dayjs from 'dayjs';

import './TimeDuration.scss';

/**
 * @component отображение даты завершения задачи
 * @param {PropType} props в компонент передаются props даты из TodoItems, где хранится состояние объекта date
 * @returns строка со значением даты выполнения задачи, которая может быть или просроченной или еще не достигнутой
 */

const TimeDuration = ({ date }) => {
  /**
   * @const currentDate получение текущей даты с использованием бибилиотеки dayjs, которая переводится в миллисекунды
   */
  const currentDate = dayjs().valueOf();

  /**
   * @const taskDate дата завершения определенной задачи, которая переводится в миллисекунды
   */
  const taskDate = Date.parse(date);
  return (
    <div className='task-date'>
      {dayjs(currentDate).diff(taskDate, 'day') <= 0 ? (
        <div className='date-completion'>
          final date:&nbsp; <span className='date-value'>{dayjs(date).format('DD MMM YYYY')}</span>
        </div>
      ) : (
        <div className='date-overdue'>time is over</div>
      )}
    </div>
  );
};

export default TimeDuration;
