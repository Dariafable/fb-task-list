import { RiCloseLine } from 'react-icons/ri';
import './ModalStyles.scss';

/**
 * @component всплывающее модальное окно, которое позволяет редактировать данные одной задачи (заголовок, описание, дату завершения, удаление файла и его скачивание)
 * @param {PropType} props в компонент передаются props из TodoItem, где хранятся данные с firebase (объект task), состояние модального окна (open,setOpen), функции редактирования задач и удаления прикрепленного файла (handleChangeTask, handleEditTask, deleteFileTask), состояние изменившихся полей задачи (newValueTask)
 * @returns возвращает компонент модального окна, в котором реализуется закрытие модального окна, редактирование inputs заголовка, описания, даты и прикрепленного файла
 */

const Modal = ({ open, setOpen, newValueTask, handleChangeTask, handleEditTask }) => {
  /**
   * @function clickSave функция, которая при клике сохраняет изменения в полях задачи и закрывает модальное окно
   */
  const clickSave = () => {
    handleEditTask();
    setOpen(false);
  };

  return (
    <div className={`overlay animated ${open ? 'show' : ''}`}>
      <div className='modal'>
        <RiCloseLine className='icon-modal' onClick={() => setOpen(false)} />

        <div className='task-wrapper'>
          <p className='modal-title'>task</p>
          <input
            className='modal-input'
            name='title'
            type='text'
            value={newValueTask.title}
            onChange={handleChangeTask}
          />
        </div>

        <div className='desc-wrapper'>
          <p className='modal-title'>description</p>
          <textarea
            className='desc-textarea'
            name='description'
            type='text'
            value={newValueTask.description}
            onChange={handleChangeTask}
          />
        </div>

        <div className='modal-title'>final date</div>
        <input
          type='date'
          name='date'
          className='modal-date'
          value={newValueTask.date}
          onChange={handleChangeTask}
        />

        <button className='save-change' onClick={clickSave}>
          save
        </button>
      </div>
    </div>
  );
};

export default Modal;
