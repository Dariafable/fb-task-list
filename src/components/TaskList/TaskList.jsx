import React from 'react';
import dayjs from 'dayjs';
import { FcReading } from 'react-icons/fc';
import { db } from '../../firebase';
import {
  query,
  collection,
  orderBy,
  serverTimestamp,
  onSnapshot,
  updateDoc,
  doc,
  addDoc,
  deleteDoc,
} from 'firebase/firestore';

import TaskItem from '../TaskItem/TaskItem';
import AddTask from '../AddTask/AddTask';

import './TaskListStyles.scss';

/**
 *
 * @component TaskList отрисовка списка добавленных задач
 */

const TaskList = () => {
  /**
   * @const {string} текущая дата
   */
  const currentDate = dayjs().format('DD MMM YYYY');

  /**
   * @const {object} tasks объект задач
   * @const {object} input значение input
   * @const {object} date дата выбора завершения задачи
   */
  const [tasks, setTasks] = React.useState([]);
  const [input, setInput] = React.useState('');
  const [date, setDate] = React.useState('');

  /**
   * получение/чтение задач с fb
   */
  React.useEffect(() => {
    const q = query(collection(db, 'tasks'), orderBy('timestamp', 'desc'));
    const unsubscribe = onSnapshot(q, (querySnapshot) => {
      let tasksArr = [];
      querySnapshot.forEach((doc) => {
        tasksArr.push({ ...doc.data(), id: doc.id });
      });
      setTasks(tasksArr);
    });
    return () => unsubscribe();
  }, []);

  /**
   * @function addTask добавляет новую задачу в список (на клиенте) и в данные fb
   * @asynk
   */
  const addTask = async (e) => {
    e.preventDefault();
    if (!input || !date) {
      alert('Please enter a valid task');
      return;
    }
    await addDoc(collection(db, 'tasks'), {
      title: input,
      completed: false,
      description: '',
      file: '',
      date: date,
      timestamp: serverTimestamp(),
    });
    setInput('');
    setDate('');
  };

  /**
   * @function changeInput получение значения input
   */
  const changeInput = ({ target }) => {
    const inputValue = target.value;
    setInput(inputValue);
  };

  /**
   * @function toggleCompleted переключает значение выполненности задачи
   * @param {object} task
   * @asynk
   */
  const toggleCompleted = async (task) => {
    await updateDoc(doc(db, 'tasks', task.id), {
      completed: !task.completed,
    });
  };

  /**
   * @function deleteTask удаляет конкретно выбранную задачу
   * @typedef id уникальный 'ключ' задачи (ID token, присваемый в fb)
   * @asynk
   */
  const deleteTask = async (id) => {
    await deleteDoc(doc(db, 'tasks', id));
  };

  /**
   * @function cleanAllTasks очищает задачи на клиентской части (данные на fb сохраняются, просто позано для примера)
   */
  const cleanAllTasks = () => {
    setTasks([]);
  };

  return (
    <div className='task-container'>
      <h1 className='task-head'>
        <FcReading />
        task list
      </h1>
      <div className='date-box'>
        <span className='current-date'>{currentDate}</span>
      </div>

      <AddTask
        input={input}
        changeInput={changeInput}
        addTask={addTask}
        date={date}
        setDate={setDate}
      />

      <ul className='all-tasks'>
        {tasks.map((task) => {
          return (
            <React.Fragment key={task.id}>
              <TaskItem task={task} toggleCompleted={toggleCompleted} deleteTask={deleteTask} />
            </React.Fragment>
          );
        })}
      </ul>

      <div className='task-bottom'>
        <span className='task-length'>
          {!tasks.length ? 'you have no tasks' : `tasks: ${tasks.length}`}
        </span>

        <button className='task-clean' onClick={cleanAllTasks}>
          clean all
        </button>
      </div>
    </div>
  );
};

export default TaskList;
